package com.restService.utils;

/**
 * Created by EDW on 3/14/2021.
 */
public class Roles {

    public static final String OTHER_SERVICE = "ROLE_OTHERS";
    public static final String SECONDARY_SERVICE = "ROLE_SECONDARY_OTHERS";
    public static final String CHEQUE_INQUIRY = "ROLE_CHEQUE_INQUIRY";

}
