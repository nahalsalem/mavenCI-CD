package com.restService.service.ws.soap.branch;

/**
 * Created by S_Salem on 10/18/2020.
 */
public class BranchParameters {

    String id;
    String baseDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBaseDate() {
        return baseDate;
    }

    public void setBaseDate(String baseDate) {
        this.baseDate = baseDate;
    }
}
